console.log("soal:no 1")

function range(startNum, finishNum){
    var rangeArray = []
    if(startNum == undefined || finishNum == undefined){
        rangeArray.push(-1);
    } else if(startNum < finishNum){
        for(var i=startNum; i<=finishNum; i++){
            rangeArray.push(i);
        }
    }
    else{
        for(var j=finishNum; j<=startNum; j++){
            rangeArray.push(j);
            rangeArray.sort(function(startNum,finishNum){
                return finishNum - startNum
            });
        }
    }
    return rangeArray;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("soal:no 2")
function rangeWithStep(startNum, finishNum,step){
    var rangeArray = []
    if(startNum < finishNum)
    {
        for(var i=startNum; i<=finishNum; i+=step){
            rangeArray.push(i);
            rangeArray.sort(function(startNum,finishNum){
                return startNum - finishNum
            });
        }
    }
    else
    {
        for(var j=finishNum; j<=startNum; j+=step){
            rangeArray.push(j);
            rangeArray.sort(function(startNum,finishNum){
                return finishNum - startNum
            });
        }
    }
    return rangeArray;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] ????

console.log("soal:no 3")

function sum(awal, akhir,jarak)
{
    var arr = []
    var total = 0
    var i
    if(awal==undefined && akhir == undefined && jarak == undefined){
        total = 0;

    }
    else if(awal==undefined || akhir == undefined && jarak == undefined)
    {
        total = 1;

    }
    else
    {
        if(jarak == undefined)
        {
            arr = rangeWithStep(awal,akhir,1)
        }
        
        else
        {
            arr = rangeWithStep(awal,akhir,jarak)
        }
        for(var i=0;i<arr.length;i++)
        {
            total +=arr[i];
        }
    }
    return total;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

